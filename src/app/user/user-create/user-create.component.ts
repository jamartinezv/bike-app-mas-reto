import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Iuser } from '../iuser';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.styl']
})
export class UserCreateComponent implements OnInit {

userFormGroup: FormGroup;

user: Iuser;

statusSearchUserId: boolean = false;

formSearchUserId = this.formBuilder.group({
  userId: ['']
})

  constructor( private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit() {
  }

  searchUserId(){
    console.warn('Data', this.formSearchUserId.value);
    this.userService.getUserByUserId(this.formSearchUserId.value.userId)
    .subscribe(res => {
      this.user = res;
      this.statusSearchUserId = true
    }, error => {
      this.user = null;
      this.statusSearchUserId = false
    });
  }
}
