import { Injectable } from '@angular/core';
import { Bike } from './interfaces/bike';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor( private http: HttpClient ) { }

  public query(): Observable<Bike[]>  {
    return this.http.get<Bike[]>(`${environment.ENN_POINT}/api/bikes`)
    .pipe(map(res => {
      return res;
    }));
  }

  public saveBike(bike: Bike): Observable<Bike>{
    return this.http.post<Bike>(`${ environment.ENN_POINT }/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }

  public getBikeById(id: String): Observable<Bike>{
    return this.http.get<Bike>(`${ environment.ENN_POINT }/api/bikes/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }

  public updateBike(bike: Bike): Observable<Bike>{
    return this.http.put<Bike>(`${ environment.ENN_POINT }/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }

  
}
