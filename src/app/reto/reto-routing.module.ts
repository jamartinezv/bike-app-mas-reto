import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuscadorComponent } from './buscador/buscador.component';
import { BuscadorListComponent } from './buscador-list/buscador-list.component';


const routes: Routes = [
  {
    path: 'buscador',
    component: BuscadorComponent
  },
  {
    path: 'buscador-list',
    component: BuscadorListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RetoRoutingModule { }
