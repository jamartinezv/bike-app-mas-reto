import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'

import { RetoRoutingModule } from './reto-routing.module';
import { BuscadorComponent } from './buscador/buscador.component';
import { BuscadorListComponent } from './buscador-list/buscador-list.component';


@NgModule({
  declarations: [BuscadorComponent, BuscadorListComponent],
  imports: [
    CommonModule,
    RetoRoutingModule,
    ReactiveFormsModule
  ]
})
export class RetoModule { }
