import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import('./components/bikes/bikes.module')
    .then(modulo => modulo.BikesModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./../app/clients/clients.module')
    .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'reto',
    loadChildren: () => import('./reto/reto.module')
    .then(mod => mod.RetoModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module')
    .then(mod => mod.UserModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
